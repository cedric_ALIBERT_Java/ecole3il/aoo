package linea;

import java.awt.Color;
import java.awt.Graphics;

//-------------------------------------------------------------------------
// ObjetGraphique : 
// classe abstraite, qui ne peut donc pas �tre instanci�e
//-------------------------------------------------------------------------
abstract class ObjetGraphique {
	//-------------------------------------------------------------------------
	// PROPRIETES
	//-------------------------------------------------------------------------
	
	// Position
	protected double x;
	protected double y;	
	
	protected Color couleur = new Color(0.0f,0.2f,0.2f);

	
	//-------------------------------------------------------------------------
	// METHODES
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------	
	// Constructeur : rien � faire
	//-------------------------------------------------------------------------	
	public ObjetGraphique(){
		
	}
	
	//-------------------------------------------------------------------------	
	// Les m�thodes d'affichage et d'animation : abstraites
	//-------------------------------------------------------------------------	
	abstract void Afficher(Graphics g);
	abstract void Animer();
	
	
	
	//-------------------------------------------------------------------------	
	// Quelques m�thodes d'acc�s 
	//-------------------------------------------------------------------------		
	public double getX(){
		return x;
	}
	
	public double getY(){
		return y;
	}
	
	
	public void setCouleur(Color c){
		couleur = c;
	}
}
