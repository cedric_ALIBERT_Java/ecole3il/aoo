package linea;

import java.awt.BasicStroke;
import java.awt.Graphics;

public class Segment extends ObjetGraphique { // h�rite de la classe ObjetGraphique
	// Ne pas oublier qu'il y a des propri�t�s et m�thodes re�ues par l'h�ritage

	//-------------------------------------------------------------------------
	// PROPRIETES
	//-------------------------------------------------------------------------

	protected double xLong;
	protected double yLong;

	
	//-------------------------------------------------------------------------
	// METHODES
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	// Constructeurs de la classe
	//-------------------------------------------------------------------------	
	public Segment(){
		x = 0; 
		y = 0;
		xLong = 50;
		yLong = 50;
	}
	
	public Segment(double xx, double yy, double xxLong, double yyLong){
		x = xx; 
		y = yy;
		xLong = xxLong;
		yLong = yyLong;		
	}

	//-------------------------------------------------------------------------	
	// Red�finition de la m�thode Afficher, sp�cifiquement pour la classe 
	//-------------------------------------------------------------------------	
	@Override
	void Afficher(Graphics g) {
		// on d�finit la couleur
		g.setColor(this.couleur);
		
		// on dessine le segment (on pourrait faire mieux...)
		g.drawLine((int)Math.round(x), (int)Math.round(y), (int)Math.round(x + xLong), (int)Math.round(y+yLong));		
	}

	//-------------------------------------------------------------------------	
	// Red�finition de la m�thode Animer, sp�cifiquement pour la classe 
	//-------------------------------------------------------------------------	
	@Override
	void Animer() {
		// TODO Auto-generated method stub
		
	}
	
	
}
