package linea;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;

import javax.swing.JPanel;

public class ZoneDessin extends JPanel { // h�rite d'une classe du frameWork standard 

	private static final long serialVersionUID = 1L;
	
	//-------------------------------------------------------------------------
	// PROPRIETES
	//-------------------------------------------------------------------------
	
	// un booleen qui permet d'arreter l'animation (suspendre)
	protected boolean estArrete = false;
	
	protected ArrayList<ObjetGraphique> listeObjets = new ArrayList<ObjetGraphique>();
	// A FAIRE : le n�cessaire pour traduire une association importante.

	
	//-------------------------------------------------------------------------
	// METHODES
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	// Constructeur de la classe
	//-------------------------------------------------------------------------	
	public ZoneDessin(){
		// on pr�pare la zone d'affichage
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(800, 600));
		setBackground(new Color(220,170,0));		
	}

	
	//-------------------------------------------------------------------------
	// Ajout d'un objet graphique � la zonde de dessin
	//-------------------------------------------------------------------------	
	public void ajouterObjet(ObjetGraphique unObjet) {
		listeObjets.add(unObjet);
	}
	
	
	//-------------------------------------------------------------------------
	// Mettre en pause et red�marrer
	//-------------------------------------------------------------------------	
	public void arreter(){
		estArrete = true;
	}
	
	public void demarrer(){
		estArrete = false;
	}
	

	//-------------------------------------------------------------------------
	// Boucle d'animation : 
	// 	- appel�e par exemple 25 fois par seconde
	//	- � chaque appel :
	//		- on anime (d�place) chaque objet
	//		- on redessine tout : appel � paintComponent, d�clench� par repaint
	//-------------------------------------------------------------------------	
	public void traiterBoucleAnimation(){
		if (estArrete==true) {
			return;
		}
		
		// 1. on d�place chaque objet graphique
		// A FAIRE : d�commenter lorsque cela devienda ex�cutable, et compl�ter
		
		for (ObjetGraphique obj : listeObjets){
			// A FAIRE : demander � obj de s'animer
			obj.Animer();
		}
		
		
		
		// 2. on demande � redessiner, ce qui d�clenchera automatiquement l'appel methode
		//    paintComponent 
		repaint();		
	}
	
	
	//-------------------------------------------------------------------------
	// Dessin, d�clench� par le repaint() de la m�thode ci-dessus 
	//-------------------------------------------------------------------------
	public void paintComponent(Graphics g) {
		// On demande � la super-classe (JPanel) de se redessiner
		super.paintComponent(g);		

		// Puis on ajoute ce qui est sp�cifique � la classe
		
		// on indique qu'il faut de l'antialiasing 
		Graphics2D g2D = (Graphics2D) g;
		g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
		
		for (ObjetGraphique obj : listeObjets){
			obj.Afficher(g);
		}
		// A FAIRE : demander � chaque objet de s'afficher
	}
	
}
	
	
