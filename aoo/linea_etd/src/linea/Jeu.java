package linea;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

public class Jeu implements KeyListener, ActionListener{
	//-------------------------------------------------------------------------
	// PROPRIETES
	//-------------------------------------------------------------------------
	
	// Ecran : instance de ZoneDessin, qui contiendra tous les objets graphiques
	// et les animera
	protected ZoneDessin ecran = new ZoneDessin();
	
	// Le cercle : en fait 2 demis-cercles, un qui passera sous la ligne
	// un qui passera dessus
	protected Cercle demiCercleAvant = new Cercle(90,-180); // celui qui est sur la ligne
	protected Cercle demiCerleArriere = new Cercle(90,180); // celui qui est derri�re la ligne
	
	// A FAIRE : ajouter les objets graphiques manquants, s'il y en a 

	// Timer : un objet qui �met des �v�nements � un intervalle choisi, 
	// il sert � donner le pas de l'animation
	protected Timer horloge;
	
	// Une variable qui contiendra le score 
	protected double score=0; 
	
	// un label qui servira � afficher le score
	protected JLabel labScore; 
	
	protected Ligne laLigne = new Ligne();
	
	
	//-------------------------------------------------------------------------
	// METHODES
	//-------------------------------------------------------------------------
		
	//-------------------------------------------------------------------------
	// Constructeur de la classe
	//-------------------------------------------------------------------------
	public Jeu(){
		// Gestion du score : a r�activer en fin de TP, inutile au d�but
		
		labScore = new JLabel();
		labScore.setText("<html><h1>score : 0</h1></html>");
		labScore.setBounds(20, 0, 200, 50);
	    ecran.add(labScore);
	    
	}
	
	
	//-------------------------------------------------------------------------
	// M�thodes qu'il faut impl�menter pour �tre 
	// conforme � un KeyListener
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	// Appui sur une touche
	// -> l'�v�nement est �mis lorsqu'on appuie, puis selon le rythme de 
	// r�p�tition du clavier
	//-------------------------------------------------------------------------
	@Override
	public  void keyPressed(KeyEvent e){
		// keyCode 38 : up
		// keyCode 40 : down
		int keyCode = e.getKeyCode();
		
		if (keyCode==38){ // touche "fl�che vers le haut"
			// On demande aux deux demi-cercle de  "monter"
			demiCercleAvant.Monter();
			demiCerleArriere.Monter();
		}		
	}

	//-------------------------------------------------------------------------
	// Rel�chement de la touche 
	//-------------------------------------------------------------------------
	@Override
	public  void keyReleased(KeyEvent e){
		// keyCode 38 : up
		// keyCode 40 : down
		int keyCode = e.getKeyCode();
		
		if (keyCode==38){
			// On demande aux deux demi-cercle "d'arr�ter de monter"
			demiCercleAvant.ArreterMonter();
			demiCerleArriere.ArreterMonter();
		}
	}

	//-------------------------------------------------------------------------
	// Une m�thode que nous n'utilisons pas
	//-------------------------------------------------------------------------
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	//-------------------------------------------------------------------------
	// D�marrage du jeu : 
	//	cr�ation de diverses instances et 
	//-------------------------------------------------------------------------
	public void demarrer(){
		// Cr�ation d'une fen�tre
		JFrame fenetre = new JFrame();
		
		// A FAIRE : 
		// placer dans l'instance de l'�cran tous les objets graphiques n�cessaires 
		// par exemple : ecran.ajouterObjet(demiCerleArriere);
		ecran.ajouterObjet(laLigne);
		ecran.ajouterObjet(demiCerleArriere);
		ecran.ajouterObjet(demiCercleAvant);

		// on indique que c'est le jeu qui traitera les appuis sur une touche
		ecran.addKeyListener(this);
		ecran.setFocusable(true);
		fenetre.setContentPane(ecran);
		fenetre.pack();
		fenetre.setLocation(100, 100);
		fenetre.setVisible(true);
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// D�marrage du timer, qui rythmera l'animation
		horloge = new Timer(40, this);
		horloge.start();
		
		// A FAIRE :
		// donner la couleur des 2 demi-cercles, par exemple :  demiCerleArriere.setCouleur(new Color(0.8f,0.0f,0.0f));
		demiCerleArriere.setCouleur(new Color(0.8f,0.0f,0.0f));
		demiCercleAvant.setCouleur(new Color(0.8f,0.0f,0.0f));
	}


	//-------------------------------------------------------------------------
	// M�thode appel�e lorsqu'un �v�nement timer se produit
	//-------------------------------------------------------------------------
	@Override
	public void actionPerformed(ActionEvent e) {
		// A FAIRE : demander � l'instance de ZoneDessin de faire "un pas d'animation"
		
		ecran.traiterBoucleAnimation();
		verif();
		
	}
	public void verif()
	{
		Segment s1 = laLigne.SegCourant;
		if(s1 != null)
		{
			double a = s1.yLong/s1.xLong;
			double x =400+(demiCercleAvant.rayon/2);
			double yseg = a*(x-s1.x)+s1.y;
			System.out.println("a:"+ a + " x:" + x + " yseg:" + yseg);
			System.out.println("yCercle:" + demiCercleAvant.y + " rayon:" + demiCercleAvant.rayon);
			
			if(((demiCercleAvant.y - demiCercleAvant.rayon) < yseg) && ((demiCercleAvant.y + demiCercleAvant.rayon) > yseg))	
			{
				score++;
				labScore.setText("<html><h1>score : "+score+"</h1></html>");
				System.out.println("+1");
			}
			else
				System.out.println("perdu");
		}
	}
		
}
