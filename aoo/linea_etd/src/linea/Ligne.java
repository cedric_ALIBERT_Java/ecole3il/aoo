package linea;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

public class Ligne extends ObjetGraphique{// h�rite de la classe ObjetGraphique
	// Ne pas oublier qu'il y a des propri�t�s et m�thodes re�ues par l'h�ritage

	//-------------------------------------------------------------------------
	// PROPRIETES
	//-------------------------------------------------------------------------	
	
	// nb de Segments qui composent la ligne
	protected int nbSegments = 400;

	// position du cercle, pour d�terminer quel est le segment courant
	protected double xCercle = 400; // � modifier
	
	// segment qui se trouve au niveau du cercle : on le m�morise pour acc�l�rer les traitements 
	protected Segment SegCourant;
	
	protected ArrayList<Segment> listeSegments = new ArrayList<Segment>();
	
	
	//-------------------------------------------------------------------------
	// METHODES
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	// Constructeur de la classe
	//-------------------------------------------------------------------------	
	public Ligne(){
		// Valeurs initiales pour la position de la Ligne
		double x = 800;
		double y = 200;
		double dx,dy;
		
		// Cr�ation de la liste de segments qui composent la ligne
		Segment s;
		for (int i=0; i<nbSegments; i++){
			// d�finition d'un nouveau segment
			dx = Math.random()*20+80;
			dy = Math.random()*40-20;
			s = new Segment(x,y,dx,dy);
			s.setCouleur(new Color(0.2f,0.2f,0.2f));
			// A FAIRE : "stocker" ce segment...
			listeSegments.add(s);
			//SegCourant = s;
			x+=dx;
			y+=dy;
		}
	}

	

	//-------------------------------------------------------------------------	
	// Red�finition de la m�thode Afficher, sp�cifiquement pour la classe Ligne
	//-------------------------------------------------------------------------
	@Override
	public void Afficher(Graphics g){
		
		// On d�finit l'�paisseur du trait
		Graphics2D g2D = (Graphics2D) g;
		g2D.setStroke(new BasicStroke(3.0f));
		for (Segment obj : listeSegments){
			obj.Afficher(g);
			if(SegCourant==null)
			{
				if(xCercle>=obj.x && xCercle<=obj.x +obj.xLong)
				{
					SegCourant = obj;
				}
			}
			else
			{
				if((SegCourant.x+SegCourant.xLong)<xCercle)
				{
					if(obj.x <= xCercle && (obj.x+obj.xLong)>=xCercle)
					{
						SegCourant = obj;
					}
				}
			}
			
		}
		// A FAIRE : programmer l'affichage de la ligne	
	}

	//-------------------------------------------------------------------------	
	// Red�finition de la m�thode Animer, sp�cifiquement pour la classe Ligne
	//-------------------------------------------------------------------------
	@Override
	public void Animer() {
		// A FAIRE : programmer l'animation de la ligne
		
		// Principe : 
		// on parcourt tous les segments et on d�cale chacun de 10 pixels (par exemple)
		// vers la gauche
		for (ObjetGraphique obj : listeSegments){
			obj.x = obj.x - 10;
		}
		
	}
}
