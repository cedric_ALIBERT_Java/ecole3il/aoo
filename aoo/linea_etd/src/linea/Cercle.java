package linea;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Arc2D;

public class Cercle extends ObjetGraphique{ // il s'agit plut�t d'arcs de cercle
	// H�rite de la classe ObjetGraphique
	// Ne pas oublier qu'il y a des propri�t�s et m�thodes re�ues par l'h�ritage

	
	//-------------------------------------------------------------------------
	// PROPRIETES
	//-------------------------------------------------------------------------	
	protected double rayon = 30;	
	protected double depY = 0; // d�placement
	
	// d�but et fin de l'arc, en degr�s
	protected double debut = 0;
	protected double fin = 360;
	
	// Est-ce que le joueur est en train d'appuyer sur "up"
	protected boolean montee = false;
	
	// Vitesse du cercle
	protected double vitesse = -1.0;
	
	// Un pas pour l'application des forces, permet de r�gler
	// un peu la jouabilit�
	protected double pas = 0.2;
	
	// Valeur de la force (norme) appliqu�e lorsque le joueur appuie sur up
	protected double impulsion = 25;
	
	
	//-------------------------------------------------------------------------
	// METHODES
	//-------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------
	// Constructeurs de la classe
	//-------------------------------------------------------------------------	
	public Cercle(){
		x=400;
		y=200;
	}
	
	public Cercle(double debutArc, double finArc){
		x=400;
		y=200;
		debut = debutArc;
		fin = finArc;
	}
	
	
	//-------------------------------------------------------------------------
	// M�thode d'acc�s en lecture au rayon, peut �tre utile
	// par exemple pour la classe qui v�rifiera si la ligne passe dans le 
	// cercle
	//-------------------------------------------------------------------------	
	public double getRayon(){
		return rayon;
	}
	
	
	//-------------------------------------------------------------------------
	// M�thode qui note que up a �t� appuy�e
	// -> le bool�en permet de ne plus �tre tributaire
	//    de la vitesse de r�p�tition du clavier
	//-------------------------------------------------------------------------		
	public void Monter(){	
		montee = true;
	}
	
	
	//-------------------------------------------------------------------------
	// M�thode qui note que up a �t� rel�ch�e
	//-------------------------------------------------------------------------	
	public void ArreterMonter(){
		montee = false;
	}
	

	
	//-------------------------------------------------------------------------	
	// Red�finition de la m�thode Afficher, sp�cifiquement pour la classe 
	//-------------------------------------------------------------------------	
	@Override
	void Afficher(Graphics g) {
		// choix de la couleur et de l'�paisseur 
		Graphics2D g2D = (Graphics2D) g;
		g2D.setStroke(new BasicStroke(5.0f));
		g.setColor(this.couleur);

		// dessin de l'arc
		g2D.draw(new Arc2D.Double(x-rayon/2, y-rayon, rayon, rayon*2, debut, fin, Arc2D.OPEN));
	}


	//-------------------------------------------------------------------------	
	// Red�finition de la m�thode Animer, sp�cifiquement pour la classe 
	//-------------------------------------------------------------------------	
	@Override
	void Animer() {
		// pas est � prendre comme un "delta t"
		
		// chute libre
		vitesse = vitesse + 9.81 * pas;

		// impulsion
		if (montee==true) {
			vitesse = vitesse - impulsion *pas;
		}
		
		depY = 1/2 * 9.81 + vitesse * pas;
		
		if (depY<-10) {
			depY=-10;
		}
		if (depY>10){
			depY =10;
		}
		y+=depY;
	}
	
	

	
}
